/*-
 * #%L
 * dynamic-discovery-client
 * %%
 * Copyright (C) 2016 - 2023 European Commission | eDelivery | Dynamic Discovery Client
 * %%
 * Licensed under the LGPL, Version 2.1 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * [PROJECT_HOME]\license\lgpl2-1\license.txt or https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
/* Copyright 2017 European Commission | CEF eDelivery
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence attached in file: LICENCE-EUPL-v1.2.pdf
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.dynamicdiscovery.core.validator;

import eu.europa.ec.dynamicdiscovery.exception.XmlInvalidAgainstSchemaException;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Joze Rihtarsic
 * @since 5.0
 */
@ExtendWith(MockitoExtension.class)
class OasisSmpSchemaValidatorTest {

    @ParameterizedTest(name = "{index}: {0}")
    @CsvSource({"service_group_urn_poland_ncpb, false, null",
            "service_metadata_unsigned_valid_iso6523, false, null",
            "service_metadata_invalid_element_added, true, 'Invalid content was found starting with element'",
            "service_metadata_element-missing, true, 'is not complete'",
            "service_group_unexpected_attribute, true, cvc-complex-type.3.2.2: Attribute 'unexpectedAttribute' is not allowed to appear in element 'ServiceMetadataReferenceCollection",
            "service_group_unsigned_invalid_iso6523_DTD-01, true, 'External DTD: Failed to read external DTD'"})
    void testOasisSMP10Validate(String xmlFilename, boolean throwsError, String errorMessage) throws IOException, XmlInvalidAgainstSchemaException, URISyntaxException {
        // given
        byte[] xmlBody = loadSMP10XMLFileAsByteArray(xmlFilename);

        XmlInvalidAgainstSchemaException result = null;
        // when
        if (throwsError) {
            result = assertThrows(XmlInvalidAgainstSchemaException.class, () -> OasisSmpSchemaValidator.validateOasisSMP10Schema(xmlBody));
            MatcherAssert.assertThat(result.getMessage(), CoreMatchers.containsString(errorMessage));
        } else {
            OasisSmpSchemaValidator.validateOasisSMP10Schema(xmlBody);
        }
        assertEquals(throwsError, result != null);
    }

    @ParameterizedTest(name = "{index}: {0}")
    @CsvSource({"service_group_unsigned_valid_iso6523, false, null",
            "service_group_unsigned_invalid_iso6523, true, 'Invalid content was found starting with element'",
            "service_group_unsigned_invalid_iso6523_DTD-01, true, 'External DTD: Failed to read external DTD'"})
    void testOasisSMP20Validate(String xmlFilename, boolean throwsError, String errorMessage) throws IOException, XmlInvalidAgainstSchemaException, URISyntaxException {
        // given
        byte[] xmlBody = loadSMP20XMLFileAsByteArray(xmlFilename);

        XmlInvalidAgainstSchemaException result = null;
        // when
        if (throwsError) {
            result = assertThrows(XmlInvalidAgainstSchemaException.class, () -> OasisSmpSchemaValidator.validateOasisSMP20ServiceGroupSchema(xmlBody));
            MatcherAssert.assertThat(result.getMessage(), CoreMatchers.containsString(errorMessage));
        } else {
            OasisSmpSchemaValidator.validateOasisSMP20ServiceGroupSchema(xmlBody);
        }
        assertEquals(throwsError, result != null);
    }

    @ParameterizedTest(name = "{index}: {0}")
    @CsvSource({"service_metadata_unsigned_valid_iso6523, false, null",
            "service_metadata_unsigned_redirection, false, null",
            "service_metadata_unsigned_invalid_iso6523, true, 'Invalid content was found starting with element'",
            "service_metadata_unsigned_invalid_iso6523_DTD-01, true, 'External DTD: Failed to read external DTD'"})
    void testOasisSMP20ServiceMetadataValidate(String xmlFilename, boolean throwsError, String errorMessage) throws IOException, XmlInvalidAgainstSchemaException, URISyntaxException {
        // given
        byte[] xmlBody = loadSMP20XMLFileAsByteArray(xmlFilename);

        XmlInvalidAgainstSchemaException result = null;
        // when
        if (throwsError) {
            result = assertThrows(XmlInvalidAgainstSchemaException.class, () -> OasisSmpSchemaValidator.validateOasisSMP20ServiceMetadataSchema(xmlBody));
            MatcherAssert.assertThat(result.getMessage(), CoreMatchers.containsString(errorMessage));
        } else {
            OasisSmpSchemaValidator.validateOasisSMP20ServiceMetadataSchema(xmlBody);
        }
        assertEquals(throwsError, result != null);
    }

    public byte[] loadSMP10XMLFileAsByteArray(String name) throws IOException, URISyntaxException {
        return Files.readAllBytes(Paths.get(OasisSmpSchemaValidator.class.getResource("/response/oasis-smp-1.0/" + name + ".xml").toURI()));
    }

    public byte[] loadSMP20XMLFileAsByteArray(String name) throws IOException, URISyntaxException {
        return Files.readAllBytes(Paths.get(OasisSmpSchemaValidator.class.getResource("/response/oasis-smp-2.0/" + name + ".xml").toURI()));
    }
}
