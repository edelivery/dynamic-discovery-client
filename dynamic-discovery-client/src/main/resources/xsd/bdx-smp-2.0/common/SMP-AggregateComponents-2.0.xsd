<?xml version="1.0" encoding="UTF-8"?>
<!--
     Service Metadata Publishing (SMP) Version 2.0
     Committee Specification Draft 01 / Public Review Draft 01
     18 July 2018
     Copyright (c) OASIS Open 2018. All Rights Reserved.
     Source: http://docs.oasis-open.org/bdxr/bdx-smp/v2.0/csprd01/xsd/common/
     Latest version of narrative specification: http://docs.oasis-open.org/bdxr/bdx-smp/v2.0/bdx-smp-v2.0.html
     TC IPR Statement: https://www.oasis-open.org/committees/bdxr/ipr.php 
-->
<!--
  Library:           OASIS Service Metadata Publisher (SMP) 2.0 2.0 CSD01WD01
                     http://docs.oasis-open.org/bdxr/bdx-smp/v2.0/csd01wd01/
  Release Date:      12 April 2018
  Module:            xsd/common/SMP-AggregateComponents-2.0.xsd
  Generated on:      2018-04-27 19:13z
  Copyright (c) OASIS Open 2018. All Rights Reserved.
-->
<xsd:schema xmlns="http://docs.oasis-open.org/bdxr/ns/SMP/2/AggregateComponents"
            xmlns:sma="http://docs.oasis-open.org/bdxr/ns/SMP/2/AggregateComponents"
            xmlns:smb="http://docs.oasis-open.org/bdxr/ns/SMP/2/BasicComponents"
            xmlns:ext="http://docs.oasis-open.org/bdxr/ns/SMP/2/ExtensionComponents"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:ccts="urn:un:unece:uncefact:documentation:2"
            targetNamespace="http://docs.oasis-open.org/bdxr/ns/SMP/2/AggregateComponents"
            elementFormDefault="qualified"
            attributeFormDefault="unqualified"
            version="2.0">
   <!-- ===== Imports ===== -->
   <xsd:import namespace="http://docs.oasis-open.org/bdxr/ns/SMP/2/BasicComponents"
               schemaLocation="SMP-BasicComponents-2.0.xsd"/>
   <xsd:import namespace="http://docs.oasis-open.org/bdxr/ns/SMP/2/ExtensionComponents"
               schemaLocation="SMP-ExtensionComponents-2.0.xsd"/>
    <!-- ==== Incorporate any desired constraints on the payload -->
    <xsd:include schemaLocation="SMP-PayloadContentDataType-2.0.xsd"/>
        
   <!-- ===== Element Declarations ===== -->
   <xsd:element name="Certificate" type="CertificateType"/>
   <xsd:element name="Endpoint" type="EndpointType"/>
   <xsd:element name="Process" type="ProcessType"/>
   <xsd:element name="ProcessMetadata" type="ProcessMetadataType"/>
   <xsd:element name="Redirect" type="RedirectType"/>
   <xsd:element name="ServiceReference" type="ServiceReferenceType"/>
   <!-- ===== Type Definitions ===== -->
   <!-- ===== Aggregate Business Information Entity Type Definitions ===== -->
   <xsd:complexType name="CertificateType">
      <xsd:annotation>
         <xsd:documentation>
            <ccts:Component>
               <ccts:ComponentType>ABIE</ccts:ComponentType>
               <ccts:DictionaryEntryName>Certificate. Details</ccts:DictionaryEntryName>
               <ccts:Definition>Definition</ccts:Definition>
               <ccts:ObjectClass>Certificate</ccts:ObjectClass>
            </ccts:Component>
         </xsd:documentation>
      </xsd:annotation>
      <xsd:sequence>
         <xsd:element ref="ext:SMPExtensions" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>A container for all extensions present in the document.</xsd:documentation>
            </xsd:annotation>
         </xsd:element>
        <xsd:element ref="smb:TypeCode" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Certificate. Type. Code</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>0..1</ccts:Cardinality>
                     <ccts:ObjectClass>Certificate</ccts:ObjectClass>
                     <ccts:PropertyTerm>Type</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Code</ccts:RepresentationTerm>
                     <ccts:DataType>Code. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="smb:Subject" minOccurs="1" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Certificate. Subject. Text</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>1</ccts:Cardinality>
                     <ccts:ObjectClass>Certificate</ccts:ObjectClass>
                     <ccts:PropertyTerm>Subject</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Text</ccts:RepresentationTerm>
                     <ccts:DataType>Text. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="smb:Issuer" minOccurs="1" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Certificate. Issuer. Text</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>1</ccts:Cardinality>
                     <ccts:ObjectClass>Certificate</ccts:ObjectClass>
                     <ccts:PropertyTerm>Issuer</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Text</ccts:RepresentationTerm>
                     <ccts:DataType>Text. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="smb:ActivationDate" minOccurs="1" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Certificate. Activation. Date</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>1</ccts:Cardinality>
                     <ccts:ObjectClass>Certificate</ccts:ObjectClass>
                     <ccts:PropertyTerm>Activation</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Date</ccts:RepresentationTerm>
                     <ccts:DataType>Date. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="smb:ExpirationDate" minOccurs="1" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Certificate. Expiration. Date</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>1</ccts:Cardinality>
                     <ccts:ObjectClass>Certificate</ccts:ObjectClass>
                     <ccts:PropertyTerm>Expiration</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Date</ccts:RepresentationTerm>
                     <ccts:DataType>Date. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="smb:ContentBinaryObject" minOccurs="1" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Certificate. Content. Binary Object</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>1</ccts:Cardinality>
                     <ccts:ObjectClass>Certificate</ccts:ObjectClass>
                     <ccts:PropertyTerm>Content</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Binary Object</ccts:RepresentationTerm>
                     <ccts:DataType>Binary Object. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
      </xsd:sequence>
   </xsd:complexType>
   <xsd:complexType name="EndpointType">
      <xsd:annotation>
         <xsd:documentation>
            <ccts:Component>
               <ccts:ComponentType>ABIE</ccts:ComponentType>
               <ccts:DictionaryEntryName>Endpoint. Details</ccts:DictionaryEntryName>
               <ccts:Definition>Definition</ccts:Definition>
               <ccts:ObjectClass>Endpoint</ccts:ObjectClass>
            </ccts:Component>
         </xsd:documentation>
      </xsd:annotation>
      <xsd:sequence>
         <xsd:element ref="ext:SMPExtensions" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>A container for all extensions present in the document.</xsd:documentation>
            </xsd:annotation>
         </xsd:element>
        <xsd:element ref="smb:TransportProfileID" minOccurs="1" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Endpoint. Transport Profile. Identifier</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>1</ccts:Cardinality>
                     <ccts:ObjectClass>Endpoint</ccts:ObjectClass>
                     <ccts:PropertyTerm>Transport Profile</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Identifier</ccts:RepresentationTerm>
                     <ccts:DataType>Identifier. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="smb:Description" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Endpoint. Description. Text</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>0..1</ccts:Cardinality>
                     <ccts:ObjectClass>Endpoint</ccts:ObjectClass>
                     <ccts:PropertyTerm>Description</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Text</ccts:RepresentationTerm>
                     <ccts:DataType>Text. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="smb:Contact" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Endpoint. Contact. Text</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>0..1</ccts:Cardinality>
                     <ccts:ObjectClass>Endpoint</ccts:ObjectClass>
                     <ccts:PropertyTerm>Contact</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Text</ccts:RepresentationTerm>
                     <ccts:DataType>Text. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="smb:AddressURI" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Endpoint. Address URI. Identifier</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>0..1</ccts:Cardinality>
                     <ccts:ObjectClass>Endpoint</ccts:ObjectClass>
                     <ccts:PropertyTerm>Address URI</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Identifier</ccts:RepresentationTerm>
                     <ccts:DataType>Identifier. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="smb:ActivationDate" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Endpoint. Activation. Date</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>0..1</ccts:Cardinality>
                     <ccts:ObjectClass>Endpoint</ccts:ObjectClass>
                     <ccts:PropertyTerm>Activation</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Date</ccts:RepresentationTerm>
                     <ccts:DataType>Date. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="smb:ExpirationDate" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Endpoint. Expiration. Date</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>0..1</ccts:Cardinality>
                     <ccts:ObjectClass>Endpoint</ccts:ObjectClass>
                     <ccts:PropertyTerm>Expiration</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Date</ccts:RepresentationTerm>
                     <ccts:DataType>Date. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="sma:Certificate" minOccurs="0" maxOccurs="unbounded">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>ASBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Endpoint. Certificate</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>0..n</ccts:Cardinality>
                     <ccts:ObjectClass>Endpoint</ccts:ObjectClass>
                     <ccts:PropertyTerm>Certificate</ccts:PropertyTerm>
                     <ccts:AssociatedObjectClass>Certificate</ccts:AssociatedObjectClass>
                     <ccts:RepresentationTerm>Certificate</ccts:RepresentationTerm>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
      </xsd:sequence>
   </xsd:complexType>
   <xsd:complexType name="ProcessType">
      <xsd:annotation>
         <xsd:documentation>
            <ccts:Component>
               <ccts:ComponentType>ABIE</ccts:ComponentType>
               <ccts:DictionaryEntryName>Process. Details</ccts:DictionaryEntryName>
               <ccts:Definition>Definition</ccts:Definition>
               <ccts:ObjectClass>Process</ccts:ObjectClass>
            </ccts:Component>
         </xsd:documentation>
      </xsd:annotation>
      <xsd:sequence>
         <xsd:element ref="ext:SMPExtensions" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>A container for all extensions present in the document.</xsd:documentation>
            </xsd:annotation>
         </xsd:element>
        <xsd:element ref="smb:ID" minOccurs="1" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Process. Identifier</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>1</ccts:Cardinality>
                     <ccts:ObjectClass>Process</ccts:ObjectClass>
                     <ccts:PropertyTerm>Identifier</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Identifier</ccts:RepresentationTerm>
                     <ccts:DataType>Identifier. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="smb:RoleID" minOccurs="0" maxOccurs="unbounded">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Process. Role. Identifier</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>0..n</ccts:Cardinality>
                     <ccts:ObjectClass>Process</ccts:ObjectClass>
                     <ccts:PropertyTerm>Role</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Identifier</ccts:RepresentationTerm>
                     <ccts:DataType>Identifier. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
      </xsd:sequence>
   </xsd:complexType>
   <xsd:complexType name="ProcessMetadataType">
      <xsd:annotation>
         <xsd:documentation>
            <ccts:Component>
               <ccts:ComponentType>ABIE</ccts:ComponentType>
               <ccts:DictionaryEntryName>Process Metadata. Details</ccts:DictionaryEntryName>
               <ccts:Definition>Definition</ccts:Definition>
               <ccts:ObjectClass>Process Metadata</ccts:ObjectClass>
            </ccts:Component>
         </xsd:documentation>
      </xsd:annotation>
      <xsd:sequence>
         <xsd:element ref="ext:SMPExtensions" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>A container for all extensions present in the document.</xsd:documentation>
            </xsd:annotation>
         </xsd:element>
        <xsd:element ref="sma:Process" minOccurs="0" maxOccurs="unbounded">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>ASBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Process Metadata. Process</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>0..n</ccts:Cardinality>
                     <ccts:ObjectClass>Process Metadata</ccts:ObjectClass>
                     <ccts:PropertyTerm>Process</ccts:PropertyTerm>
                     <ccts:AssociatedObjectClass>Process</ccts:AssociatedObjectClass>
                     <ccts:RepresentationTerm>Process</ccts:RepresentationTerm>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="sma:Endpoint" minOccurs="0" maxOccurs="unbounded">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>ASBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Process Metadata. Endpoint</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>0..n</ccts:Cardinality>
                     <ccts:ObjectClass>Process Metadata</ccts:ObjectClass>
                     <ccts:PropertyTerm>Endpoint</ccts:PropertyTerm>
                     <ccts:AssociatedObjectClass>Endpoint</ccts:AssociatedObjectClass>
                     <ccts:RepresentationTerm>Endpoint</ccts:RepresentationTerm>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="sma:Redirect" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>ASBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Process Metadata. Redirect</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>0..1</ccts:Cardinality>
                     <ccts:ObjectClass>Process Metadata</ccts:ObjectClass>
                     <ccts:PropertyTerm>Redirect</ccts:PropertyTerm>
                     <ccts:AssociatedObjectClass>Redirect</ccts:AssociatedObjectClass>
                     <ccts:RepresentationTerm>Redirect</ccts:RepresentationTerm>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
      </xsd:sequence>
   </xsd:complexType>
   <xsd:complexType name="RedirectType">
      <xsd:annotation>
         <xsd:documentation>
            <ccts:Component>
               <ccts:ComponentType>ABIE</ccts:ComponentType>
               <ccts:DictionaryEntryName>Redirect. Details</ccts:DictionaryEntryName>
               <ccts:Definition>Definition</ccts:Definition>
               <ccts:ObjectClass>Redirect</ccts:ObjectClass>
            </ccts:Component>
         </xsd:documentation>
      </xsd:annotation>
      <xsd:sequence>
         <xsd:element ref="ext:SMPExtensions" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>A container for all extensions present in the document.</xsd:documentation>
            </xsd:annotation>
         </xsd:element>
        <xsd:element ref="smb:PublisherURI" minOccurs="1" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Redirect. Publisher URI. Identifier</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>1</ccts:Cardinality>
                     <ccts:ObjectClass>Redirect</ccts:ObjectClass>
                     <ccts:PropertyTerm>Publisher URI</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Identifier</ccts:RepresentationTerm>
                     <ccts:DataType>Identifier. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="sma:Certificate" minOccurs="0" maxOccurs="unbounded">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>ASBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Redirect. Certificate</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>0..n</ccts:Cardinality>
                     <ccts:ObjectClass>Redirect</ccts:ObjectClass>
                     <ccts:PropertyTerm>Certificate</ccts:PropertyTerm>
                     <ccts:AssociatedObjectClass>Certificate</ccts:AssociatedObjectClass>
                     <ccts:RepresentationTerm>Certificate</ccts:RepresentationTerm>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
      </xsd:sequence>
   </xsd:complexType>
   <xsd:complexType name="ServiceReferenceType">
      <xsd:annotation>
         <xsd:documentation>
            <ccts:Component>
               <ccts:ComponentType>ABIE</ccts:ComponentType>
               <ccts:DictionaryEntryName>Service Reference. Details</ccts:DictionaryEntryName>
               <ccts:Definition>Definition</ccts:Definition>
               <ccts:ObjectClass>Service Reference</ccts:ObjectClass>
            </ccts:Component>
         </xsd:documentation>
      </xsd:annotation>
      <xsd:sequence>
         <xsd:element ref="ext:SMPExtensions" minOccurs="0" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>A container for all extensions present in the document.</xsd:documentation>
            </xsd:annotation>
         </xsd:element>
        <xsd:element ref="smb:ID" minOccurs="1" maxOccurs="1">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>BBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Service Reference. Identifier</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>1</ccts:Cardinality>
                     <ccts:ObjectClass>Service Reference</ccts:ObjectClass>
                     <ccts:PropertyTerm>Identifier</ccts:PropertyTerm>
                     <ccts:RepresentationTerm>Identifier</ccts:RepresentationTerm>
                     <ccts:DataType>Identifier. Type</ccts:DataType>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
         <xsd:element ref="sma:Process" minOccurs="0" maxOccurs="unbounded">
            <xsd:annotation>
               <xsd:documentation>
                  <ccts:Component>
                     <ccts:ComponentType>ASBIE</ccts:ComponentType>
                     <ccts:DictionaryEntryName>Service Reference. Process</ccts:DictionaryEntryName>
                     <ccts:Definition>Definition</ccts:Definition>
                     <ccts:Cardinality>0..n</ccts:Cardinality>
                     <ccts:ObjectClass>Service Reference</ccts:ObjectClass>
                     <ccts:PropertyTerm>Process</ccts:PropertyTerm>
                     <ccts:AssociatedObjectClass>Process</ccts:AssociatedObjectClass>
                     <ccts:RepresentationTerm>Process</ccts:RepresentationTerm>
                  </ccts:Component>
               </xsd:documentation>
            </xsd:annotation>
         </xsd:element>
      </xsd:sequence>
   </xsd:complexType>
</xsd:schema>
<!-- ===== Copyright Notice ===== -->
<!--
  OASIS takes no position regarding the validity or scope of any 
  intellectual property or other rights that might be claimed to pertain 
  to the implementation or use of the technology described in this 
  document or the extent to which any license under such rights 
  might or might not be available; neither does it represent that it has 
  made any effort to identify any such rights. Information on OASIS's 
  procedures with respect to rights in OASIS specifications can be 
  found at the OASIS website. Copies of claims of rights made 
  available for publication and any assurances of licenses to be made 
  available, or the result of an attempt made to obtain a general 
  license or permission for the use of such proprietary rights by 
  implementors or users of this specification, can be obtained from 
  the OASIS Executive Director.

  OASIS invites any interested party to bring to its attention any 
  copyrights, patents or patent applications, or other proprietary 
  rights which may cover technology that may be required to 
  implement this specification. Please address the information to the 
  OASIS Executive Director.
  
  This document and translations of it may be copied and furnished to 
  others, and derivative works that comment on or otherwise explain 
  it or assist in its implementation may be prepared, copied, 
  published and distributed, in whole or in part, without restriction of 
  any kind, provided that the above copyright notice and this 
  paragraph are included on all such copies and derivative works. 
  However, this document itself may not be modified in any way, 
  such as by removing the copyright notice or references to OASIS, 
  except as needed for the purpose of developing OASIS 
  specifications, in which case the procedures for copyrights defined 
  in the OASIS Intellectual Property Rights document must be 
  followed, or as required to translate it into languages other than 
  English. 

  The limited permissions granted above are perpetual and will not be 
  revoked by OASIS or its successors or assigns. 

  This document and the information contained herein is provided on 
  an "AS IS" basis and OASIS DISCLAIMS ALL WARRANTIES, 
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY 
  WARRANTY THAT THE USE OF THE INFORMATION HEREIN 
  WILL NOT INFRINGE ANY RIGHTS OR ANY IMPLIED 
  WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A 
  PARTICULAR PURPOSE.    
-->
