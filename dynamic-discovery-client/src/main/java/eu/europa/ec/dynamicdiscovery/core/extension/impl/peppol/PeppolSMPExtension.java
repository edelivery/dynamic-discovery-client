/*
 * #%L
 * dynamic-discovery-cli
 * %%
 * Copyright (C) 2016 - 2023 European Commission | eDelivery | Dynamic Discovery Client
 * %%
 * Licensed under the LGPL, Version 2.1 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * [PROJECT_HOME]\license\lgpl2-1\license.txt or https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package eu.europa.ec.dynamicdiscovery.core.extension.impl.peppol;

import eu.europa.ec.dynamicdiscovery.core.extension.impl.AbstractExtension;

import java.util.Arrays;

/**
 * Peppol SMP extension providing the ServiceGroup and SignedServiceMetadata parser
 *
 * @author Cosmin Baciu
 * @since 2.1
 */
public class PeppolSMPExtension extends AbstractExtension {

    public static final String NAMESPACE = "http://busdox.org/serviceMetadata/publishing/1.0/";

    final PeppolSMPServiceGroupReader serviceGroupReader;
    final PeppolSMPServiceMetadataReader serviceMetadataReader;

    public PeppolSMPExtension() {
        this(false);
    }

    public PeppolSMPExtension(boolean ignoreInvalidServices) {
        serviceGroupReader = new PeppolSMPServiceGroupReader();
        serviceMetadataReader = new PeppolSMPServiceMetadataReader(ignoreInvalidServices);
        parsers = Arrays.asList(serviceGroupReader, serviceMetadataReader);
    }

    public void setIgnoreInvalidServices(boolean ignoreInvalidServices) {
        this.serviceMetadataReader.setIgnoreInvalidServices(ignoreInvalidServices);
    }
}
